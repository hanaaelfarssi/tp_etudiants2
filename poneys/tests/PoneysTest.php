<?php
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {
    public function test_removePoneyFromField() {
      // Setup
      $Poneys = new Poneys();

      // Action

      $Poneys->removePoneyFromField(3);

      // Assert
      $this->assertEquals(5, $Poneys->getCount());

      
    }

/**
     * @expectedException Exception       
     * @expectedExceptionMessage Negatif
     */
public function test2_removePoneyFromField() {
      // Setup
      $Poneys = new Poneys();

      // Action

      $Poneys->removePoneyFromField(9);

      // Assert
      $this->assertEquals(5, $Poneys->getCount());


    }
/**
     * @dataProvider removeProvider
     */

public function test3_removePoneyFromField($a, $expected) {
      // Setup
      $Poneys = new Poneys();

      // Action

      $Poneys->removePoneyFromField($a);

      // Assert
       $this->assertEquals($expected, $Poneys->getCount());


    }
public function removeProvider()
    {
        return [
            [1, 7],
            [4, 4],
           
        ];
    }
public function test_getNames(){
$Poneys = new Poneys();
$Poneys = $this->getMockBuilder('Poneys')->getMock();
$Poneys-> expects($this->exactly(1))
       ->method('getNames')
       ->willReturn(array('Joe','Wiliam','Jack','Averell')
       );
$this->assertEquals(
array('Joe','Wiliam','Jack','Averell'),
$Poneys-> getNames()

);

}

public function setUp(){
$Poneys = new Poneys();
$Poneys-> setCount(15);
  }

public function tearDown(){

unset($this->Poneys);
}

}
 ?>
